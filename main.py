from flask import Flask
from app import views

app = Flask(__name__)

# url
app.add_url_rule('/base', 'base', views.base)
app.add_url_rule('/', 'index', views.index)
app.add_url_rule('/objectdetectapp', 'objectdetectapp', views.object_detect_app)
app.add_url_rule('/objectdetectapp/upload', 'upload', views.upload, methods=['GET', 'POST'])
app.add_url_rule('/objectdetectapp/upload/report_download', 'report_download', views.report_download,
                 methods=['GET', 'POST'])
app.add_url_rule('/page', 'page', views.page)
app.add_url_rule('/api', 'api', views.api)

if __name__ == "__main__":
    app.run(debug=True)
