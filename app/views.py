from flask import render_template, request, send_file, jsonify
import os
from app.imageutil import object_detect_in_image
from app.videoutils import object_detect_in_video, report_generate_video

UPLOAD_FOLDER = 'static/uploads'
REPORT_FOLDER = 'static/report'


def base():
    return render_template('base.html')


def index():
    return render_template('index.html')


def object_detect_app():
    return render_template('objectdetectapp.html')


def page():
    return render_template('page.html')


def upload():
    if request.method == "POST":
        f = request.files['image']
        filename = f.filename
        print(filename)
        if filename[-3:] == 'jpg':
            path = os.path.join(UPLOAD_FOLDER, filename)
            f.save(path)
            object_detect_in_image(path, filename, color='bgr')

            return render_template('upload.html', fileupload=True, img_name=filename)

        if filename[-3:] == 'mp4':
            path = os.path.join(UPLOAD_FOLDER, filename)
            f.save(path)
            object_detect_in_video(path, filename, color='bgr')
            report_generate_video()

            return render_template('upload.html', videoupload=True, img_name=filename)

    return render_template('upload.html', fileupload=False, img_name="freeai.png")


def report_download():
    if request.method == "GET":
        path = 'report.docx'

        return send_file(path, as_attachment=True)


def api():
    f = open("E:/Git Projects/Python Project/ObjectDetectionSystemFlask/temp.txt", "r")
    r = f.read()
    print(r)
    a: str = r
    if a.find('laptop') != -1:
        l = True
    else:
        l = False

    if a.find('person') != -1:
        p = True
    else:
        p = False

    if a.find('chair') != -1:
        c = True
    else:
        c = False

    result = {
        "Laptop Exist": l,
        "Person Exist": p,
        "Chair Exist": c,
    }
    return jsonify(result)
